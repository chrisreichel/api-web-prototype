package com.sentenial.origix.api.service;

import com.sentenial.origix.api.bean.Payment;
import org.springframework.stereotype.Service;

/**
 * Copyright of Sentenial
 * User: christian.reichel
 * Date: 26/02/14
 * Time: 16:54
 */
@Service
public class PaymentServiceImpl implements PaymentService {

    @Override
    public Payment create(Payment payment) {
        payment.setId(payment.getId() + 1);
        return payment;
    }

    @Override
    public Payment get(Long id) {
        final Payment payment = new Payment();
        payment.setId(payment.getId() + 1);
        return payment;
    }

    @Override
    public void remove(Payment payment) {
        payment.setId(-1L);
    }
}
