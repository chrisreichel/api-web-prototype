package com.sentenial.origix.api.service;

import com.sentenial.origix.api.bean.Payment;

/**
 * Copyright of Sentenial
 * User: christian.reichel
 * Date: 26/02/14
 * Time: 16:54
 */
public interface PaymentService {

    Payment create(Payment payment);

    Payment get(Long id);

    void remove(Payment payment);

}
