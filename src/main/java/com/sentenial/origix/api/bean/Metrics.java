package com.sentenial.origix.api.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Copyright of Sentenial
 * User: christian.reichel
 * Date: 30/04/2014
 * Time: 12:39
 */
public class Metrics implements Serializable {

    private String id;
    private String user;
    private Long shownOverlay;
    private Long apply;
    private List<Long> requote;
    private Long end;
    private Long firstNo;
    private Long secondNo;
    private List<Quote> quotes;
    private List<Quotation> quotations;

    public Long getShownOverlay() {
        return shownOverlay;
    }

    public void setShownOverlay(Long shownOverlay) {
        this.shownOverlay = shownOverlay;
    }

    public Long getEnd() {
        return end;
    }

    public void setEnd(Long end) {
        this.end = end;
    }

    public Long getFirstNo() {
        return firstNo;
    }

    public void setFirstNo(Long firstNo) {
        this.firstNo = firstNo;
    }

    public Long getSecondNo() {
        return secondNo;
    }

    public void setSecondNo(Long secondNo) {
        this.secondNo = secondNo;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getApply() {
        return apply;
    }

    public void setApply(Long apply) {
        this.apply = apply;
    }

    public List<Long> getRequote() {
        return requote;
    }

    public void setRequote(List<Long> requote) {
        this.requote = requote;
    }

    public List<Quote> getQuotes() {
        return quotes;
    }

    public void setQuotes(List<Quote> quotes) {
        this.quotes = quotes;
    }

    public List<Quotation> getQuotations() {
        return quotations;
    }

    public void setQuotations(List<Quotation> quotations) {
        this.quotations = quotations;
    }

    @Override
    public String toString() {
        return "Metrics{" +
                "id='" + id + '\'' +
                ", user='" + user + '\'' +
                ", shownOverlay=" + shownOverlay +
                ", apply=" + apply +
                ", requote=" + requote +
                ", end=" + end +
                ", firstNo=" + firstNo +
                ", secondNo=" + secondNo +
                ", quotes=" + quotes +
                ", quotations=" + quotations +
                '}';
    }
}
