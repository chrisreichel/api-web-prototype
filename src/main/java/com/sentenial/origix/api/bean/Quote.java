package com.sentenial.origix.api.bean;

import java.io.Serializable;

/**
 * Copyright of Sentenial
 * User: christian.reichel
 * Date: 30/04/2014
 * Time: 10:06
 */
public class Quote implements Serializable {

    private Double monthlyPayment;
    private Double finalValue;
    private Double rate;

    public Double getMonthlyPayment() {
        return monthlyPayment;
    }

    public void setMonthlyPayment(Double monthlyPayment) {
        this.monthlyPayment = monthlyPayment;
    }

    public Double getFinalValue() {
        return finalValue;
    }

    public void setFinalValue(Double finalValue) {
        this.finalValue = finalValue;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    @Override
    public String toString() {
        return "Quote{" +
                "monthlyPayment=" + monthlyPayment +
                ", finalValue=" + finalValue +
                ", rate=" + rate +
                '}';
    }
}
