package com.sentenial.origix.api.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

/**
 * Copyright of Sentenial
 * User: christian.reichel
 * Date: 26/02/14
 * Time: 16:34
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Payment implements Serializable {

    private Long id;
    private Date date;
    private Double value;
    private String description;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
