package com.sentenial.origix.api.bean;

import java.io.Serializable;

/**
 * Copyright of Sentenial
 * User: christian.reichel
 * Date: 30/04/2014
 * Time: 11:47
 */
public class Quotation implements Serializable {

    private Quote requested;

    private Double amount;
    private Integer terms;

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public void setAmount(String amount) {
        this.amount = Double.parseDouble(amount);
    }

    public Integer getTerms() {
        return terms;
    }

    public void setTerms(Integer terms) {
        this.terms = terms;
    }

    public void setTerms(String terms) {
        this.terms = Integer.parseInt(terms);
    }

    public Quote getRequested() {
        return requested;
    }

    public void setRequested(Quote requested) {
        this.requested = requested;
    }

    @Override
    public String toString() {
        return "Quotation{" +
                "requested=" + requested +
                ", amount=" + amount +
                ", terms=" + terms +
                '}';
    }
}
