package com.sentenial.origix.api.rest;

import com.sentenial.origix.api.bean.Payment;
import com.sentenial.origix.api.service.PaymentService;
import org.apache.cxf.jaxrs.model.wadl.Description;
import org.apache.cxf.jaxrs.model.wadl.Descriptions;
import org.apache.cxf.jaxrs.model.wadl.DocTarget;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.*;
import java.util.Date;

/**
 * Copyright of Sentenial
 * User: christian.reichel
 * Date: 26/02/14
 * Time: 16:33
 */
@Path("/v1")
@Produces({"application/xml", "application/json"})
public class PaymentRestAPI {

    @Autowired
    private PaymentService paymentService;

    @GET
    @Path("/payment/{id}/")
    public Payment getPayment(@PathParam("id") String id) {
        System.out.println("----invoking getPayment, Payment id is: " + id);
        long idNumber = Long.parseLong(id);
        final Payment payment = new Payment();
        payment.setId(idNumber);
        return payment;
    }

    @DELETE
    @Path("/payment/{id}/")
    public void deletePayment(@PathParam("id") String id) {
        System.out.println("----invoking deletePayment, Payment id is: " + id);
    }

    @POST
    @Path("/payment")
    public Payment createPayment() {
        final Payment payment = new Payment();
        return payment;
    }

    @Descriptions({
            @Description(value = "Just a test for a Payment", target = DocTarget.METHOD),
            @Description(value = "Requested Payment", target = DocTarget.RETURN),
            @Description(value = "Request", target = DocTarget.REQUEST),
            @Description(value = "Response", target = DocTarget.RESPONSE),
            @Description(value = "Resource", target = DocTarget.RESOURCE)
    })
    @GET
    @Path("/payment")
    public Payment test() {
        final Payment payment = new Payment();
        payment.setDate(new Date());
        payment.setDescription("Hello");
        payment.setValue(12.34);
        return payment;
    }

}
