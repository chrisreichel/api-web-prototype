package com.sentenial.origix.api.web;

import com.sentenial.origix.api.bean.Payment;
import com.sentenial.origix.api.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Copyright of Sentenial
 * User: christian.reichel
 * Date: 26/02/14
 * Time: 16:38
 */
@Controller
public class PaymentController {

    @Autowired
    private PaymentService paymentService;

    @ModelAttribute("payment")
    public Payment getPaymentForm() {
        return new Payment();
    }

    @RequestMapping(value = "/stripe", method = RequestMethod.GET)
    public String getStripe() {
        return "stripe";
    }

    @RequestMapping(value = "/v1/payment", method = RequestMethod.GET)
    public String getPaymentInput(ModelMap map) {
        return "payment/input";
    }

    @RequestMapping(value = "/v1/payment", method = RequestMethod.POST)
    public String createPayment(@ModelAttribute("payment") Payment payment) {
        return "payment/done";
    }
}
