package com.sentenial.origix.api.web;

import com.sentenial.origix.api.bean.Metrics;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * Copyright of Sentenial
 * User: christian.reichel
 * Date: 30/04/2014
 * Time: 12:38
 */
@Controller
public class MetricsController {

    @RequestMapping(value = "/v1/metrics", method = RequestMethod.POST)
    @ResponseBody
    public void collectMetric(@ModelAttribute Metrics metrics, HttpServletRequest request) {
        System.out.println("Metric collected: " + metrics.toString());
    }

}
