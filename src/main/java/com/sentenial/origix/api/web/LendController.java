package com.sentenial.origix.api.web;

import com.sentenial.origix.api.bean.Quotation;
import com.sentenial.origix.api.bean.Quote;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * Copyright of Sentenial
 * User: christian.reichel
 * Date: 30/04/2014
 * Time: 09:54
 */
@Controller
public class LendController {

    @RequestMapping(value = "/v1/lend", method = RequestMethod.GET)
    public String getLendPage(HttpServletRequest request) {
        System.out.println("Asking form for lending");
        return "lend/input";
    }

    @RequestMapping(value = "/v1/lend/quote", method = RequestMethod.POST)
    @ResponseBody
    public Quote quote(@ModelAttribute Quotation quotation, HttpServletRequest request) {
        System.out.println("Sending lending data: " + quotation);
        final Quote quote = new Quote();
        quotation.setRequested(quote);
        quote.setFinalValue(3.456);
        quote.setMonthlyPayment(123.01);
        quote.setRate(9.0);
        return quote;
    }
}
