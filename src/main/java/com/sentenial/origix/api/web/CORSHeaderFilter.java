package com.sentenial.origix.api.web;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Copyright of Sentenial
 * User: christian.reichel
 * Date: 29/04/2014
 * Time: 09:28
 *
 * http://en.wikipedia.org/wiki/Cross-origin_resource_sharing
 *
 */
public class CORSHeaderFilter implements Filter {

    public void init(FilterConfig filterConfig) throws ServletException {}

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        ((HttpServletResponse)response).addHeader(
                "Access-Control-Allow-Origin", "*"
        );
        chain.doFilter(request, response);
    }

    public void destroy() {}
}
