<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <title>Stripe Getting Started Form</title>

    <!-- The required Stripe lib -->
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
</head>
<body>
<h1>Charge $10 with Stripe</h1>

<form action="" method="POST" id="payment-form">
    <script src="<spring:url value='/static/js/sent-dd.js'/>" class="stripe-button"
            data-key="pk_test_cKbuJbyPyisLjamEZJ2rOuM3"
            data-amount="2000"
            data-name="Demo Site"
            data-description="2 widgets ($20.00)"
            data-image="<spring:url value='/static/images/top-logo.png'/>">
    </script>
</form>
<hr/>
<h1>Another stripe method</h1>
<form action="" method="POST">
    <script src="https://checkout.stripe.com/checkout.js" class="stripe-button"
            data-key="pk_test_cKbuJbyPyisLjamEZJ2rOuM3"
            data-amount="2000"
            data-name="Demo Site"
            data-description="2 widgets ($20.00)"
            data-image="<spring:url value='/static/images/top-logo.png'/>">
    </script>
</form>


</body>
</html>