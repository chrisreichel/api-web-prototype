<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html lang="en">
<head>
    <title>Input</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.0/jquery.min.js"/>
    <script type="text/javascript" src="<spring:url value='/static/js/bootstrap.js'/>"></script>
    <link rel="stylesheet" type="text/css" href="<spring:url value='/static/css/bootstrap.css'/>">
    <link rel="stylesheet" type="text/css" href="<spring:url value='/static/css/bootstrap-theme.css'/>">
    <script language="JavaScript" type="text/javascript">
        $(document).ready(function() {

            var timing = {};
            timing.shownOverlay = new Date().getTime();
            timing.requote = [];
            timing.quotes = [];
            timing.quotations = [];
            timing.user = "testing";
            timing.id = "unique1";

            $('#myModal').on('shown.bs.modal', function() {
                console.log("launched");
            })

            $("#quote-me1").on("click", function(){
                var url = "<spring:url value='/web/v1/lend/quote'/>";
                var params = {};
                params.terms = $("#months").val();
                params.amount = $("#amount").val();
                timing.quotes.push(params);
                var jqxhr = $.post(url, params, function(data) {
                    $(".step1").hide();
                    $(".step2").show();
                    console.log("Success! " + JSON.stringify(data));
                    $("#monthlyPayment").text(data.monthlyPayment);
                    $("#totalPayment").text(data.finalValue);
                    $("#rate").text(data.rate + " %");
                    $("#loanTerms").text(params.terms + " months");
                    $("#loanAmount").text("€ " + params.amount);
                    timing.quotations.push(data);
                })
                .done(function() {
                    console.log( "second success ?" );
                })
                .fail(function() {
                    console.log( "error on quote" );
                })
                .always(function() {
                    console.log( "finished quering the quote" );
                    console.log("---> " + JSON.stringify(timing));
                });
            });

            $("#apply-btn").on("click", function(){
                $(".step2").hide();
                $(".step3").show();
                timing.apply= new Date().getTime();
                console.log("---> " + JSON.stringify(timing));
                sendMetrics(timing);
            });

            $("#not-now-btn").on("click", function(){
                $('#myPaymentBody').hide();
                timing.firstNo = new Date().getTime();
                console.log("---> " + JSON.stringify(timing));
                sendMetrics(timing);
            });

            $("#no-thx-btn").on("click", function(){
                $('#myPaymentBody').hide();
                timing.secondNo = new Date().getTime();
                sendMetrics(timing);
            });

            $("#end-btn").on("click", function(){
                $('#myPaymentBody').hide();
                timing.end = new Date().getTime();
                console.log("---> " + JSON.stringify(timing));
                sendMetrics(timing);
            });

            $("#requote-me").on("click", function(){
                $(".step2").hide();
                $(".step1").show();
                timing.requote.push(new Date().getTime());
                console.log("---> " + JSON.stringify(timing));
            });

            function sendMetrics(timing){
                var metricsURL = "<spring:url value='/web/v1/metrics'/>";
                $.post(metricsURL, timing);
            }
        });
    </script>
    <style>
        .enclosing-border{
            border: 2px solid blue;
            position: absolute;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            /*background-color: lightgrey;*/
            border-radius: 5px;
            -moz-opacity: 0.7; /* makes the div transparent, so you have a cool overlay effect */
            opacity: .70;
            filter: alpha(opacity=70);
            z-index: 10;
        }
        .select-small {
            width: 150px; !important;
        }
        .input-small {
            width: 150px; !important;
        }
        .show-grid [class^="col-"] {
            background-color: rgba(86, 61, 124, 0.15);
            border: 1px solid rgba(86, 61, 124, 0.2);
            padding-bottom: 5px;
            padding-top: 5px;
        }
        #myModal {
            margin: -300px 0 0 -280px;
        }
    </style>
</head>
    <body id="myPaymentBody" class="container">
    <input type="hidden" id="myModalStart">
    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog enclosing-border">
            <div class="modal-content">
                <div class="modal-header">
                    <img src="<spring:url value='/static/images/top-logo.png'/>"/>
                </div>
                <div class="modal-body container">
                    <div id="step1" class="row step1">
                        <div class="form-group">
                            <label for="amount" class="control-label">Amount</label>
                            <select id="amount" class="form-control input-sm select-small" name="amount">
                                <option value="0"></option>
                                <option value="1000">€ 1,000</option>
                                <option value="2000">€ 2,000</option>
                                <option value="3000">€ 3,000</option>
                                <option value="4000">€ 4,000</option>
                                <option value="5000">€ 5,000</option>
                                <option value="10000">€ 10,000</option>
                                <option value="20000">€ 20,000</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="months" class="control-label">For how long</label>
                            <select id="months" class="form-control input-sm select-small" name="amount">
                                <option value="0"></option>
                                <option value="3">3 Months</option>
                                <option value="6">6 Months</option>
                                <option value="9">9 Months</option>
                                <option value="12">12 Months</option>
                                <option value="18">18 Months</option>
                                <option value="24">24 Months</option>
                                <option value="36">36 Months</option>
                            </select>
                        </div>
                    </div>
                    <div id="step2" class="step2 show-grid" style="display: none">
                        <div class="row">
                            <div class="col-xs-3 pull-right">Loan Amount:</div>
                            <div class="col-xs-2" id="loanAmount"></div>
                            <div class="col-xs-3">Organization name:</div>
                            <div class="col-xs-4">
                                <input type="text" class="form-control input-sm input-small" name="organization" id ="organization">
                            </div>
                        </div>
                        <div class="row"><div class="col-xs-offset-12"></div></div>
                        <div class="row">
                            <div class="col-xs-3">Loan terms:</div>
                            <div class="col-xs-2" id="loanTerms"></div>
                            <div class="col-xs-3">Your name:</div>
                            <div class="col-xs-4">
                                <input type="text" class="form-control input-sm input-small" name="name" id ="name">
                            </div>
                        </div>
                        <div class="row"><div class="col-xs-offset-12"></div></div>
                        <div class="row">
                            <div class="col-xs-3">Rate of interest:</div>
                            <div class="col-xs-2" id="rate"></div>
                            <div class="col-xs-3">Your position:</div>
                            <div class="col-xs-4">
                                <select id="position" class="form-control input-sm select-small">
                                    <option>Owner</option>
                                    <option>Employee</option>
                                </select>
                            </div>
                        </div>
                        <div class="row"><div class="col-xs-offset-12"></div></div>
                        <div class="row">
                            <div class="col-xs-3">Montly repayment:</div>
                            <div class="col-xs-2" id="monthlyPayment"></div>
                            <div class="col-xs-3">Your phone number:</div>
                            <div class="col-xs-4">
                                <input type="text" class="form-control input-sm input-small" name="phone" id ="phone">
                            </div>
                        </div>
                        <div class="row"><div class="col-xs-offset-12"></div></div>
                        <div class="row">
                            <div class="col-xs-3">Total interest:</div>
                            <div class="col-xs-2" id="totalPayment"></div>
                            <div class="col-xs-3">Your email:</div>
                            <div class="col-xs-4">
                                <input type="email" class="form-control input-sm input-small" name="email" id ="email">
                            </div>
                        </div>
                        <div class="row"><div class="col-xs-offset-12"></div></div>
                    </div>
                    <div id="step3" class="row step3 show-grid" style="display: none">
                        <div class="row">
                            <div class="col-xs-12 ">Thank you for you application.</div>
                            <div class="col-xs-12 ">Your request has ben registered (Ref #1000091).</div>
                            <div class="col-xs-12 ">You will be contacted by Sentenial within 24 hours.</div>
                            <div class="col-xs-offset-12"></div>
                            <div class="col-xs-12 ">In case of any queries please contact <a>loans@sentenial.com</a> with above Ref ID.</div>
                        </div>
                        <div class="row"><div class="col-xs-offset-12"></div></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="step1 row show-grid">
                        <div class="col-xs-2">
                            <button id="not-now-btn" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Not now</button>
                        </div>
                        <div class="col-xs-2">
                            <button id="quote-me1" class="btn btn-default" aria-hidden="true">Quote me</button>
                        </div>
                        <div class="col-xs-8"></div>
                    </div>
                    <div class="step2 row show-grid" style="display: none">
                        <div class="col-xs-4">
                            <button id="requote-me" class="btn btn-default" aria-hidden="true">Requote me</button>
                        </div>
                        <div class="col-xs-4">
                            <button id="apply-btn" class="btn btn-primary" aria-hidden="true">Apply</button>
                        </div>
                        <div class="col-xs-4">
                            <button id="no-thx-btn" class="btn btn-default" data-dismiss="modal" aria-hidden="true">No thanks</button>
                        </div>
                    </div>
                    <div class="step3 row show-grid" style="display: none">
                        <button id="end-btn" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Exit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </body>
</html>