<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html lang="en">
<head>
    <title>Input</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.4.2/pure-min.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.0/jquery.min.js"/>
    <script language="JavaScript" type="text/javascript">
        function proceed(){
            alert("What?");
            jQuery("#confirmation").show();
            jQuery("#form").hide();
        }
    </script>
    <style>
        .fade {
            /*display: none;*/
            border: 2px solid black;
            position: absolute;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            background-color: black;
            border-radius: 5px;
            -moz-opacity: 0.7; /* makes the div transparent, so you have a cool overlay effect */
            opacity: .70;
            filter: alpha(opacity=70);
            z-index: 10;
        }

        .overlay {
            /* display: none; */
            border: 1px solid #222222;
            width: 460px;
            height: 400px;
            position: fixed;
            top: 50%;
            left: 50%;
            margin-top: -300px;
            margin-left: -250px;
            background-color: #f5f5f5;
            border-radius: 5px;
            text-align: center;
            z-index: 11; /* 1px higher than the overlay layer */
        }
        .right
        {
            text-align: right;
        }
        .left
        {
            text-align: left;
        }
    </style>
</head>
<body id="myPaymentBody">
    <div class="fade"></div>
    <div class="overlay pure-g-r" id="sent_payment_container">
        <div id="form">
            <form class="pure-form">
                <fieldset>
                    <legend></legend>
                    <div class="pure-u-1-1">
                        <input type="text" name="name" id="name" placeholder="Name">
                    </div>
                    <div class="pure-u-1-1">
                        <input type="text" name="surname" id="surname" placeholder="Surname">
                    </div>
                    <div class="pure-u-1-1">
                        <input type="text" name="sort" id="account" placeholder="Account Number">
                    </div>
                    <div class="pure-u-1-1">
                        <input type="text" name="sort" id="sort" placeholder="Sort Code">
                    </div>
                    <div class="pure-u-1-1 right">
                        <button type="button" id="proceed" onclick="javascript:proceed();"  class="pure-button pure-button-secondary">Proceed</button>
                    </div>
                </fieldset>
            </form>
        </div>
        <div id="confirmation" style="display: none">
            <legend></legend>
            <div class="pure-u-1-1">
                Account holder: <pre>J Bond</pre>
            </div>
            <div class="pure-u-1-1">
                Account number: <pre>997988</pre>
            </div>
            <div class="pure-u-1-1">
                SortCode: <pre>23-45-99</pre>
            </div>
            <div class="pure-u-1-1">
                Value: $ <pre>9.99</pre>
            </div>
            <div class="pure-u-1-1 right">
                <button type="button" id="finish" class="pure-button pure-button-secondary">Finish</button>
            </div>
        </div>
    </div>
</body>
</html>