<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>

<h1>
  <bean:message key="formdata.accessdenied"/>
</h1>

<br/><br/>

<center>
	<p>
		<bean:message key="formdata.accessdeniedmessage"/>
	</p>
</center>

<br/><br/><br/>
