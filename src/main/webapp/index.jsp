<%
response.setHeader("Cache-Control", "no-cache");
response.setHeader("Pragma", "no-cache");
response.setDateHeader("max-age", 0);
response.setDateHeader("Expires", 0);
%>

<%@ page language="java" %>

<h1>REST API</h1>
<a href="/api-web-module/api/v1/payment">/api/v1/payment</a>
<br/>
<a href="/api-web-module/api/v1/payment?_wadl">Payment API Documentation (WADL)</a>
<br/>
<hr/>

<h1>WEB API</h1>
<a href="/api-web-module/web/v1/payment">/web/v1/payment</a>