(function () {

    jQuery('<button id="checkoutBtn" type="button" class="btn btn-success">Checkout</button>').appendTo("#buttonGoesHere");

    jQuery('<iframe />', {
        name: 'paymentFrame',
        id:   'paymentFrame',
        src: 'http://localhost:8080/api-web-module/web/v1/payment',
        frameborder: 0,
        style: "z-index: 9999; display: none; background: none repeat scroll 0% 0% transparent; border: 0px none transparent; overflow-x: hidden; overflow-y: auto; visibility: visible; margin: 0px; padding: 0px; position: fixed; left: 0px; top: 0px; width: 100%; height: 100%;"
    }).appendTo('body');

    jQuery("#checkoutBtn").click(function(){
        //jQuery("#paymentFrame").fadeIn();
        jQuery("#paymentFrame").fadeIn();
    });

    jQuery("#confirmation").hide();

    function getIframeWindow(iframe_object) {
        var doc;

        if (iframe_object.contentWindow) {
            return iframe_object.contentWindow;
        }

        if (iframe_object.window) {
            return iframe_object.window;
        }

        if (!doc && iframe_object.contentDocument) {
            doc = iframe_object.contentDocument;
        }

        if (!doc && iframe_object.document) {
            doc = iframe_object.document;
        }

        if (doc && doc.defaultView) {
            return doc.defaultView;
        }

        if (doc && doc.parentWindow) {
            return doc.parentWindow;
        }

        return undefined;
    }

    var iFrame = document.getElementById('paymentFrame');

    getIframeWindow(el).proceed();

})();

