(function () {

    setTimeout(function(){
        //jQuery('#lendFrame').css('opacity', .5);
        var ref = jQuery("#lendFrame");
        var src = ref.attr('data-src');
        var height = ref.attr('data-height') || 400;
        var width = ref.attr('data-width') || 720;

        jQuery("#smeLendModal iframe").attr({'src':src,
            'height': height,
            'width': width});
        jQuery('#myModal').modal('show');
    }, 1000);

})();

